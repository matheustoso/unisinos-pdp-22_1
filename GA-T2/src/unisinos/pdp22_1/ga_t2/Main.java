package unisinos.pdp22_1.ga_t2;

import unisinos.pdp22_1.ga_t2.Linguagem.Language;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            System.out.println("--------------MENU INICIAL--------------");
            System.out.println("Digite o caminho e o nome do arquivo que deseja");
            System.out.println("Ou digite 0 para sair");
            String input = scanner.nextLine();
            if (input.equals("")) {
                System.out.println("Opção inválida!");
            } else {
                if (input.equals("0")) {
                    scanner.close();
                    return;
                } else {
                    readFile(input);
                }
            }
        }
    }

    static void readFile(String fileName) {
        try {
            FileReader fr = new FileReader(fileName);
            BufferedReader in = new BufferedReader(fr);
            String line = in.readLine();
            while (line != null) {
                Language.executar(line);
                line = in.readLine();
            }
            in.close();
        } catch (FileNotFoundException e) {
            System.out.println("Arquivo \"" + fileName + "\" não existe.");
        } catch (IOException e) {
            System.out.println("Erro na leitura de " + fileName + ".");
        }
    }

    static void checkLine(String line) {
        int end = line.indexOf(";");
        line = line.substring(0, end);
        String[] parts = line.split(" ");
        System.out.println(Arrays.toString(parts));
    }
}
