package unisinos.pdp22_1.ga_t2.Linguagem.Posicao;

import unisinos.pdp22_1.ga_t2.Linguagem.Tokens.TiposToken;

public class Posicao {
    private int index;
    private int linha;
    private int coluna;
    private final String nomeArquivo;
    private final String conteudoArquivo;

    public Posicao(int index, int linha, int coluna, String nomeArquivo, String conteudoArquivo) {
        this.index = index;
        this.linha = linha;
        this.coluna = coluna;
        this.nomeArquivo = nomeArquivo;
        this.conteudoArquivo = conteudoArquivo;
    }

    public Posicao avancar(){
        return avancar('\0');
    }

    public Posicao avancar(char charAtual) {
        setIndex(getIndex() + 1);
        setColuna(getColuna() + 1);
        if (Character.toString(charAtual).equals(TiposToken.NOVA_LINHA.valor)) {
            setLinha(getLinha() + 1);
            setColuna(0);
        }

        return this;
    }

    public Posicao copiar() {
        return new Posicao(getIndex(), getLinha(), getColuna(), getNomeArquivo(), getConteudoArquivo());
    }

    public int getIndex() {
        return this.index;
    }

    public int getLinha() {
        return this.linha;
    }

    public int getColuna() {
        return this.coluna;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    public String getConteudoArquivo() {
        return conteudoArquivo;
    }

    private void setIndex(int index) {
        this.index = index;
    }

    private void setLinha(int linha) {
        this.linha = linha;
    }

    private void setColuna(int coluna) {
        this.coluna = coluna;
    }
}
