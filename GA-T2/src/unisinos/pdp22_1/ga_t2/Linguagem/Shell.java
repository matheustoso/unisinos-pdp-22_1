package unisinos.pdp22_1.ga_t2.Linguagem;

import unisinos.pdp22_1.ga_t2.Linguagem.Language;

import java.util.Scanner;

public class Shell {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            System.out.println("write stuff: ");
            String texto = scanner.nextLine();
            Language.executar(texto);
        }
    }

}
