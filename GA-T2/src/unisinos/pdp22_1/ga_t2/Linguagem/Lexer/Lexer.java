package unisinos.pdp22_1.ga_t2.Linguagem.Lexer;

import unisinos.pdp22_1.ga_t2.Linguagem.Erros.ErroCharIlegal;
import unisinos.pdp22_1.ga_t2.Linguagem.Posicao.Posicao;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.Resultado;
import unisinos.pdp22_1.ga_t2.Linguagem.Tokens.PalavrasChave;
import unisinos.pdp22_1.ga_t2.Linguagem.Tokens.TiposToken;
import unisinos.pdp22_1.ga_t2.Linguagem.Tokens.Token;

import java.util.ArrayList;

public class Lexer {
    private final String nomeArquivo;
    private final String texto;
    private Posicao posicaoAtual;
    private char charAtual;

    public Lexer(String texto, String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
        this.texto = texto;
        this.posicaoAtual = new Posicao(-1, 0, -1, nomeArquivo, texto);
        this.charAtual = '\0';
    }

    public void avancar() {
        posicaoAtual.avancar(charAtual);
        if (posicaoAtual.getIndex() < texto.length()) {
            charAtual = texto.charAt(posicaoAtual.getIndex());
        } else {
            charAtual = '\0';
        }
    }

    public Resultado<ArrayList<Token>> criarTokens() {
        ArrayList<Token> tokens = new ArrayList<>();
        avancar();

        while (charAtual != '\0') {
            if (TiposToken.VAZIO.valor.contains(Character.toString(charAtual))) {
                avancar();
            } else if (TiposToken.NUMERO.valor.contains(Character.toString(charAtual))) {
                Resultado<Token> resultado = criarNumero();
                if (resultado.getErro() != null) return new Resultado<>(new ArrayList<>(), resultado.getErro());
                tokens.add(resultado.getValor());
            } else if (Character.toString(charAtual).matches(TiposToken.ID.valor)) {
                Resultado<Token> resultado = criarIdentificador();
                if (resultado.getErro() != null) return new Resultado<>(new ArrayList<>(), resultado.getErro());
                tokens.add(resultado.getValor());
            } else if (TiposToken.MAIS.valor.contains(Character.toString(charAtual))) {
                tokens.add(new Token(TiposToken.MAIS, posicaoAtual));
                avancar();
            } else if (TiposToken.MENOS.valor.contains(Character.toString(charAtual))) {
                tokens.add(new Token(TiposToken.MENOS, posicaoAtual));
                avancar();
            } else if (TiposToken.MULT.valor.contains(Character.toString(charAtual))) {
                tokens.add(new Token(TiposToken.MULT, posicaoAtual));
                avancar();
            } else if (TiposToken.DIV.valor.contains(Character.toString(charAtual))) {
                tokens.add(new Token(TiposToken.DIV, posicaoAtual));
                avancar();
            } else if (TiposToken.POT.valor.contains(Character.toString(charAtual))) {
                tokens.add(new Token(TiposToken.POT, posicaoAtual));
                avancar();
            } else if (TiposToken.ATRIBUICAO.valor.contains(Character.toString(charAtual))) {
                tokens.add(new Token(TiposToken.ATRIBUICAO, posicaoAtual));
                avancar();
            } else if (TiposToken.PARENTESE_ESQUERDO.valor.contains(Character.toString(charAtual))) {
                tokens.add(new Token(TiposToken.PARENTESE_ESQUERDO, posicaoAtual));
                avancar();
            } else if (TiposToken.PARENTESE_DIREITO.valor.contains(Character.toString(charAtual))) {
                tokens.add(new Token(TiposToken.PARENTESE_DIREITO, posicaoAtual));
                avancar();
            }else if (TiposToken.NEGACAO.valor.contains(Character.toString(charAtual))) {
                Resultado<Token> resultado = criarNegacao();
                if (resultado.getErro() != null) return new Resultado<>(new ArrayList<>(), resultado.getErro());
                tokens.add(resultado.getValor());
            } else if (TiposToken.IGUAL.valor.contains(Character.toString(charAtual))) {
                tokens.add(new Token(TiposToken.IGUAL, posicaoAtual));
                avancar();
            }else if (TiposToken.MAIOR.valor.contains(Character.toString(charAtual))){
                Resultado<Token> resultado = criarMaior();
                if (resultado.getErro() != null) return new Resultado<>(new ArrayList<>(), resultado.getErro());
                tokens.add(resultado.getValor());
            }else if (TiposToken.MENOR.valor.contains(Character.toString(charAtual))){
                Resultado<Token> resultado = criarMenor();
                if (resultado.getErro() != null) return new Resultado<>(new ArrayList<>(), resultado.getErro());
                tokens.add(resultado.getValor());
            }else if (TiposToken.E.valor.contains(Character.toString(charAtual))){
                tokens.add(new Token(TiposToken.E, posicaoAtual));
                avancar();
            }else if (TiposToken.OU.valor.contains(Character.toString(charAtual))){
                tokens.add(new Token(TiposToken.OU, posicaoAtual));
                avancar();
            }else if (TiposToken.XOU.valor.contains(Character.toString(charAtual))){
                tokens.add(new Token(TiposToken.XOU, posicaoAtual));
                avancar();
            }else if (TiposToken.VIRGULA.valor.contains(Character.toString(charAtual))){
                tokens.add(new Token(TiposToken.VIRGULA, posicaoAtual));
                avancar();
            } else {
                Posicao posicaoInicial = posicaoAtual.copiar();
                char charIlegal = charAtual;
                avancar();
                return new Resultado<>(new ArrayList<>(),
                                       new ErroCharIlegal("'" + charIlegal + "'", posicaoInicial, posicaoAtual));
            }
        }

        tokens.add(new Token(TiposToken.FIM_DO_ARQUIVO, posicaoAtual, posicaoAtual.copiar().avancar()));
        return new Resultado<>(tokens, null);
    }

    public Resultado<Token> criarNumero() {
        StringBuilder numero = new StringBuilder();
        int countPonto = 0;
        String valores = TiposToken.NUMERO.valor + ".";
        Posicao posicaoInicial = posicaoAtual.copiar();

        while (charAtual != '\0' && valores.contains(Character.toString(charAtual))) {
            if (charAtual == '.') {
                countPonto++;
                if (countPonto > 1) {
                    posicaoInicial = posicaoAtual.copiar();
                    avancar();
                    return new Resultado<>(null,
                                           new ErroCharIlegal("Número possui segundo ponto",
                                                              posicaoInicial,
                                                              posicaoAtual));
                }
            }
            numero.append(charAtual);
            avancar();
        }

        return new Resultado<>(new Token(TiposToken.NUMERO, numero.toString(), posicaoInicial, posicaoAtual), null);
    }

    public Resultado<Token> criarIdentificador() {
        StringBuilder identificador = new StringBuilder();
        Posicao posicaoInicial = posicaoAtual.copiar();

        while (this.charAtual != '\0' && Character.toString(charAtual).matches(TiposToken.ID.valor)) {
            identificador.append(charAtual);
            avancar();
        }

        TiposToken tipo = PalavrasChave.getValores().contains(identificador.toString())
                          ? TiposToken.CHAVE
                          : TiposToken.ID;

        return new Resultado<>(new Token(tipo, identificador.toString(), posicaoInicial, posicaoAtual), null);
    }

    public Resultado<Token> criarNegacao() {
        Posicao posicaoInicial = posicaoAtual.copiar();
        avancar();

        if (TiposToken.IGUAL.valor.contains(Character.toString(charAtual))){
            avancar();
            return new Resultado<>(new Token(TiposToken.DIFERENTE, TiposToken.DIFERENTE.valor, posicaoInicial, posicaoAtual), null);
        }

        return new Resultado<>(new Token(TiposToken.NEGACAO, TiposToken.NEGACAO.valor, posicaoInicial, posicaoAtual), null);
    }

    public Resultado<Token> criarMaior() {
        Posicao posicaoInicial = posicaoAtual.copiar();
        TiposToken tipo = TiposToken.MAIOR;
        avancar();

        if(TiposToken.IGUAL.valor.contains(Character.toString(charAtual))){
            avancar();
            tipo = TiposToken.MAIOR_IGUAL;
        }

        return new Resultado<>(new Token(tipo, tipo.valor, posicaoInicial, posicaoAtual), null);
    }

    public Resultado<Token> criarMenor() {
        Posicao posicaoInicial = posicaoAtual.copiar();
        TiposToken tipo = TiposToken.MENOR;
        avancar();

        if(TiposToken.IGUAL.valor.contains(Character.toString(charAtual))){
            avancar();
            tipo = TiposToken.MENOR_IGUAl;
        }

        return new Resultado<>(new Token(tipo, tipo.valor, posicaoInicial, posicaoAtual), null);
    }
}
