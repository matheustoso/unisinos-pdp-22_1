package unisinos.pdp22_1.ga_t2.Linguagem.Tokens;

public enum TiposToken {
    VAZIO(" \t"),
    NOVA_LINHA("\n"),
    NUMERO("0123456789"),
    MAIS("+"),
    MENOS("-"),
    DIV("/"),
    MULT("*"),
    POT("^"),
    E("&"),
    OU("|"),
    NEGACAO("!"),
    XOU(":"),
    PARENTESE_ESQUERDO("("),
    PARENTESE_DIREITO(")"),
    CHAVE(""),
    ID("^[a-zA-Z_]*$"),
    ATRIBUICAO("$"),
    IGUAL("="),
    DIFERENTE("!="),
    MAIOR(">"),
    MENOR("<"),
    MAIOR_IGUAL(">="),
    MENOR_IGUAl("<="),
    VIRGULA(","),
    FIM_DO_ARQUIVO("");
    public final String valor;

    TiposToken(String valor) {
        this.valor = valor;
    }

}
