package unisinos.pdp22_1.ga_t2.Linguagem.Tokens;

import unisinos.pdp22_1.ga_t2.Linguagem.Nos.No;
import unisinos.pdp22_1.ga_t2.Linguagem.Posicao.Posicao;

import java.math.BigDecimal;

public class Token extends No {
    private TiposToken tipo;
    private String valor;

    public Token(TiposToken tipo, Posicao posicaoInicial) {
        super(posicaoInicial.copiar(), posicaoInicial.copiar().avancar());
        this.tipo = tipo;
        this.valor = tipo.valor;
    }

    public Token(TiposToken tipo, Posicao posicaoInicial, Posicao posicaoFinal) {
        super(posicaoInicial.copiar(), posicaoFinal.copiar());
        this.tipo = tipo;
        this.valor = tipo.valor;
    }

    public Token(TiposToken tipo, String valor, Posicao posicaoInicial, Posicao posicaoFinal) {
        super(posicaoInicial.copiar(), posicaoFinal.copiar());
        this.tipo = tipo;
        this.valor = valor;
    }

    public boolean checaIgualdade(TiposToken tipo, String valor) {
        return getTipo().equals(tipo) && getValor().equals(valor);
    }

    public Posicao getPosicaoInicial() {
        return super.getPosicaoInicial();
    }

    public Posicao getPosicaoFinal() {
        return super.getPosicaoFinal();
    }

    public TiposToken getTipo() {
        return tipo;
    }

    public Object getValor() {
        return toTipo(valor);
    }

    private Object toTipo(String valor) {
        return switch (tipo) {
            case NUMERO -> new BigDecimal(valor);
            default -> valor;
        };
    }

    @Override
    public String toString() {
        return getTipo().toString() + "::" + getValor();
    }
}
