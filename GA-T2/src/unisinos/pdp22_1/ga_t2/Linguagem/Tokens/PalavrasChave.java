package unisinos.pdp22_1.ga_t2.Linguagem.Tokens;

import java.util.ArrayList;

public enum PalavrasChave {
    VARIAVEL("var");

    public final String valor;

    PalavrasChave(String valor) {
        this.valor = valor;
    }

    public static ArrayList<String> getValores() {
        ArrayList<String> valores = new ArrayList<>();
        for (PalavrasChave p : PalavrasChave.values()) {
            valores.add(p.valor);
        }
        return valores;
    }
}
