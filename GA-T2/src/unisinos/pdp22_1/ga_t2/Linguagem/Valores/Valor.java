package unisinos.pdp22_1.ga_t2.Linguagem.Valores;

import unisinos.pdp22_1.ga_t2.Linguagem.Contexto.Contexto;
import unisinos.pdp22_1.ga_t2.Linguagem.Posicao.Posicao;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.ResultadoExecucao;

public abstract class Valor {
    private Posicao posicaoInicial;
    private Posicao posicaoFinal;
    private Contexto contexto;

    public Valor() {
        this.posicaoInicial = null;
        this.posicaoFinal = null;
        this.contexto = null;
    }

    public Valor setContexto(Contexto contexto) {
        this.contexto = contexto;
        return this;
    }

    public void setPosicao(Posicao posicaoInicial, Posicao posicaoFinal) {
        this.posicaoInicial = posicaoInicial;
        this.posicaoFinal = posicaoFinal;
    }

    public abstract ResultadoExecucao somadoCom(Numero numero);

    public abstract ResultadoExecucao subtraidoPor(Numero numero);

    public abstract ResultadoExecucao multiplicadoPor(Numero numero);

    public abstract ResultadoExecucao divididoPor(Numero numero);

    public abstract ResultadoExecucao potenciadoPor(Numero numero);

    public abstract ResultadoExecucao igual(Numero numero);

    public abstract ResultadoExecucao diferente(Numero numero);

    public abstract ResultadoExecucao maior(Numero numero);

    public abstract ResultadoExecucao menor(Numero numero);

    public abstract ResultadoExecucao maiorIgual(Numero numero);

    public abstract ResultadoExecucao menorIgual(Numero numero);

    public abstract ResultadoExecucao e(Numero numero);

    public abstract ResultadoExecucao ou(Numero numero);

    public abstract ResultadoExecucao xou(Numero numero);

    public abstract ResultadoExecucao negado();

    public abstract ResultadoExecucao paraNegativo();

    public abstract Valor copiar();

    public Posicao getPosicaoInicial() {
        return posicaoInicial;
    }

    public Posicao getPosicaoFinal() {
        return posicaoFinal;
    }

    public Contexto getContexto() {
        return contexto;
    }
}
