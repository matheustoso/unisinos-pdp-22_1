package unisinos.pdp22_1.ga_t2.Linguagem.Valores;

import unisinos.pdp22_1.ga_t2.Linguagem.Contexto.Contexto;
import unisinos.pdp22_1.ga_t2.Linguagem.Erros.ErroDeExecucao;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.ResultadoExecucao;
import unisinos.pdp22_1.ga_t2.Linguagem.Simbolos;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Funcao extends Valor {
    public static final Funcao _if = new Funcao("if");
    public static final Funcao sum = new Funcao("sum");
    public static final Funcao avg = new Funcao("avg");
    public static final Funcao max = new Funcao("max");
    public static final Funcao min = new Funcao("min");
    public static final Funcao abs = new Funcao("abs");
    public static final Funcao sqrt = new Funcao("sqrt");
    public static final Funcao rand = new Funcao("rand");
    private final String nome;

    public Funcao(String nome) {
        this.nome = nome;
    }

    @Override
    public Funcao copiar() {
        Funcao copia = new Funcao(nome);
        copia.setContexto(getContexto());
        copia.setPosicao(getPosicaoInicial(), getPosicaoFinal());
        return copia;
    }

    public Contexto gerarNovoContexto() {
        Contexto novoContexto = new Contexto(nome, getPosicaoInicial(), getContexto());
        novoContexto.setSimbolos(new Simbolos(novoContexto.getPai().getSimbolos()));
        return novoContexto;
    }

    private ResultadoExecucao checarParametros(ArrayList<String> nomes, ArrayList<Valor> parametros) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        if (!nomes.get(nomes.size() - 1).equals("*") && parametros.size() > nomes.size()) {
            return resultado.falha(new ErroDeExecucao(parametros.size() - nomes.size() + " parâmetros além do máximo",
                                                      getPosicaoInicial(),
                                                      getPosicaoFinal()));
        }

        if (parametros.size() < nomes.size()) {
            return resultado.falha(new ErroDeExecucao(nomes.size() - parametros.size() + " parâmetros faltando",
                                                      getPosicaoInicial(),
                                                      getPosicaoFinal()));
        }

        return resultado.sucesso(null);
    }

    private void popularParametros(ArrayList<String> nomes,
                                   ArrayList<Valor> parametros,
                                   Contexto contexto) {
        boolean contemParametrosAnonimos = false;

        int j = 0;
        for (int i = 0; i < parametros.size(); i++) {
            if (!contemParametrosAnonimos && nomes.get(i).equals("*")) {
                contemParametrosAnonimos = true;
            }
            if (contemParametrosAnonimos) {
                j++;
            }
            String nome = contemParametrosAnonimos ? "*" + j : nomes.get(i);

            Valor valor = parametros.get(i);
            valor.setContexto(contexto);
            contexto.getSimbolos().permanecer(nome, valor);
        }
    }

    public ResultadoExecucao checarPopularParametros(ArrayList<String> nomes,
                                                     ArrayList<Valor> parametros,
                                                     Contexto contexto) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        resultado.registrar(checarParametros(nomes, parametros));
        if (resultado.getErro() != null) return resultado;
        popularParametros(nomes, parametros, contexto);
        return resultado.sucesso(null);
    }

    public ResultadoExecucao executar(ArrayList<Valor> parametros) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        Contexto contexto = gerarNovoContexto();
        System.out.println(nome);
        Valor valor;
        switch (nome) {
            case "if":
                resultado.registrar(checarPopularParametros(getNomesIf(), parametros, contexto));
                if (resultado.getErro() != null) return resultado;
                valor = resultado.registrar(executarIf(contexto));
                if (resultado.getErro() != null) return resultado;
                return resultado.sucesso(valor);
            case "sum":
                resultado.registrar(checarPopularParametros(getNomesSum(), parametros, contexto));
                if (resultado.getErro() != null) return resultado;
                valor = resultado.registrar(executarSum(contexto, parametros.size()));
                if (resultado.getErro() != null) return resultado;
                return resultado.sucesso(valor);
            case "avg":
                resultado.registrar(checarPopularParametros(getNomesAvg(), parametros, contexto));
                if (resultado.getErro() != null) return resultado;
                valor = resultado.registrar(executarAvg(contexto, parametros.size()));
                if (resultado.getErro() != null) return resultado;
                return resultado.sucesso(valor);
            case "max":
                resultado.registrar(checarPopularParametros(getNomesMax(), parametros, contexto));
                if (resultado.getErro() != null) return resultado;
                valor = resultado.registrar(executarMax(contexto, parametros.size()));
                if (resultado.getErro() != null) return resultado;
                return resultado.sucesso(valor);
            case "min":
                resultado.registrar(checarPopularParametros(getNomesMin(), parametros, contexto));
                if (resultado.getErro() != null) return resultado;
                valor = resultado.registrar(executarMin(contexto, parametros.size()));
                if (resultado.getErro() != null) return resultado;
                return resultado.sucesso(valor);
            case "abs":
                resultado.registrar(checarPopularParametros(getNomesAbs(), parametros, contexto));
                if (resultado.getErro() != null) return resultado;
                valor = resultado.registrar(executarAbs(contexto));
                if (resultado.getErro() != null) return resultado;
                return resultado.sucesso(valor);
            case "sqrt":
                resultado.registrar(checarPopularParametros(getNomesSqrt(), parametros, contexto));
                if (resultado.getErro() != null) return resultado;
                valor = resultado.registrar(executarSqrt(contexto));
                if (resultado.getErro() != null) return resultado;
                return resultado.sucesso(valor);
            case "rand":
                resultado.registrar(checarPopularParametros(getNomesRand(), parametros, contexto));
                if (resultado.getErro() != null) return resultado;
                valor = resultado.registrar(executarRand(contexto));
                if (resultado.getErro() != null) return resultado;
                return resultado.sucesso(valor);
            default:
                break;
        }

        return resultado.falha(new ErroDeExecucao("Função " + nome + " não existe",
                                                  getPosicaoInicial(),
                                                  getPosicaoFinal()));
    }

    private ResultadoExecucao executarRand(Contexto contexto) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        Valor maximo = contexto.getSimbolos().obter("maximo");
        Numero multiplicador = new Numero(BigDecimal.valueOf(Math.random()));
        Valor rand = resultado.registrar(maximo.multiplicadoPor(multiplicador));

        return resultado.sucesso(rand);
    }

    private ResultadoExecucao executarSqrt(Contexto contexto) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        Valor numero = contexto.getSimbolos().obter("numero");
        Valor absoluto = (resultado.registrar(numero.potenciadoPor(new Numero(new BigDecimal("0.5")))));
        if (resultado.getErro() != null) return resultado.falha(new ErroDeExecucao("Raiz inválida",
                                                                                   numero.getPosicaoInicial(),
                                                                                   numero.getPosicaoFinal()));

        return resultado.sucesso(absoluto);
    }

    private ResultadoExecucao executarAbs(Contexto contexto) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        Numero numero = (Numero) contexto.getSimbolos().obter("numero");
        Numero absoluto = new Numero(numero.getValor().abs());

        return resultado.sucesso(absoluto);
    }

    private ResultadoExecucao executarMin(Contexto contexto, int qtdParametros) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        Valor minimo = contexto.getSimbolos().obter("numero");
        Numero temp;
        for (int i = 1; i < qtdParametros; i++) {
            temp = (Numero) contexto.getSimbolos().obter("*" + i);
            Valor comparacao = resultado.registrar(minimo.maior(temp));
            if (comparacao.equals(Numero.TRUE)) minimo = temp;
        }
        return resultado.sucesso(minimo);
    }

    private ResultadoExecucao executarMax(Contexto contexto, int qtdParametros) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        Valor maximo = contexto.getSimbolos().obter("numero");
        Numero temp;
        for (int i = 1; i < qtdParametros; i++) {
            temp = (Numero) contexto.getSimbolos().obter("*" + i);
            Valor comparacao = resultado.registrar(maximo.menor(temp));
            if (comparacao.equals(Numero.TRUE)) maximo = temp;
        }
        return resultado.sucesso(maximo);
    }

    private ResultadoExecucao executarAvg(Contexto contexto, int qtdParametros) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        Valor soma = contexto.getSimbolos().obter("numero");
        for (int i = 1; i < qtdParametros; i++) {
            soma = resultado.registrar(soma.somadoCom((Numero) contexto.getSimbolos().obter("*" + i)));
        }

        Valor media = resultado.registrar(soma.divididoPor(new Numero(new BigDecimal(qtdParametros))));
        return resultado.sucesso(media);
    }

    private ResultadoExecucao executarSum(Contexto contexto, int qtdParametros) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        Valor soma = contexto.getSimbolos().obter("numero");
        for (int i = 1; i < qtdParametros; i++) {
            soma = resultado.registrar(soma.somadoCom((Numero) contexto.getSimbolos().obter("*" + i)));
        }
        return resultado.sucesso(soma);
    }

    private ResultadoExecucao executarIf(Contexto contexto) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        Numero condicao = (Numero) contexto.getSimbolos().obter("condicao");
        Numero verdadeiro = (Numero) contexto.getSimbolos().obter("verdadeiro");
        Numero falso = (Numero) contexto.getSimbolos().obter("falso");
        Numero numero = condicao.getValor().equals(Numero.TRUE.getValor()) ? verdadeiro : falso;

        return resultado.sucesso(numero);
    }

    @Override
    public String toString() {
        return "<func " + nome + ">";
    }

    public ArrayList<String> getNomesIf() {
        ArrayList<String> nomes = new ArrayList<>();
        nomes.add("condicao");
        nomes.add("verdadeiro");
        nomes.add("falso");

        return nomes;
    }

    public ArrayList<String> getNomesSum() {
        ArrayList<String> nomes = new ArrayList<>();
        nomes.add("numero");
        nomes.add("*");

        return nomes;
    }

    public ArrayList<String> getNomesAvg() {
        ArrayList<String> nomes = new ArrayList<>();
        nomes.add("numero");
        nomes.add("*");

        return nomes;
    }

    public ArrayList<String> getNomesMax() {
        ArrayList<String> nomes = new ArrayList<>();
        nomes.add("numero");
        nomes.add("*");

        return nomes;
    }

    public ArrayList<String> getNomesMin() {
        ArrayList<String> nomes = new ArrayList<>();
        nomes.add("numero");
        nomes.add("*");

        return nomes;
    }

    public ArrayList<String> getNomesAbs() {
        ArrayList<String> nomes = new ArrayList<>();
        nomes.add("numero");

        return nomes;
    }

    public ArrayList<String> getNomesSqrt() {
        ArrayList<String> nomes = new ArrayList<>();
        nomes.add("numero");

        return nomes;
    }

    public ArrayList<String> getNomesRand() {
        ArrayList<String> nomes = new ArrayList<>();
        nomes.add("maximo");

        return nomes;
    }


    @Override
    public ResultadoExecucao somadoCom(Numero numero) {
        return null;
    }

    @Override
    public ResultadoExecucao subtraidoPor(Numero numero) {
        return null;
    }

    @Override
    public ResultadoExecucao multiplicadoPor(Numero numero) {
        return null;
    }

    @Override
    public ResultadoExecucao divididoPor(Numero numero) {
        return null;
    }

    @Override
    public ResultadoExecucao potenciadoPor(Numero numero) {
        return null;
    }

    @Override
    public ResultadoExecucao igual(Numero numero) {
        return null;
    }

    @Override
    public ResultadoExecucao diferente(Numero numero) {
        return null;
    }

    @Override
    public ResultadoExecucao maior(Numero numero) {
        return null;
    }

    @Override
    public ResultadoExecucao menor(Numero numero) {
        return null;
    }

    @Override
    public ResultadoExecucao maiorIgual(Numero numero) {
        return null;
    }

    @Override
    public ResultadoExecucao menorIgual(Numero numero) {
        return null;
    }

    @Override
    public ResultadoExecucao e(Numero numero) {
        return null;
    }

    @Override
    public ResultadoExecucao ou(Numero numero) {
        return null;
    }

    @Override
    public ResultadoExecucao xou(Numero numero) {
        return null;
    }

    @Override
    public ResultadoExecucao negado() {
        return null;
    }

    @Override
    public ResultadoExecucao paraNegativo() {
        return null;
    }
}
