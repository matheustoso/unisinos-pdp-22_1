package unisinos.pdp22_1.ga_t2.Linguagem.Valores;

import unisinos.pdp22_1.ga_t2.Linguagem.Erros.ErroDeExecucao;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.ResultadoExecucao;

import java.math.BigDecimal;
import java.math.MathContext;

public class Numero extends Valor {
    public static final Numero FALSE = new Numero(BigDecimal.ZERO, true);
    public static final Numero TRUE = new Numero(BigDecimal.ONE, true);
    private BigDecimal valor;
    private boolean booleano;
    private static MathContext mathContext = MathContext.DECIMAL128;

    public Numero(BigDecimal valor) {
        super();
        this.valor = valor;
        this.booleano = false;
    }

    public Numero(BigDecimal valor, boolean bool) {
        super();
        this.valor = valor;
        this.booleano = bool;
    }

    public void setBooleano() {
        this.booleano = true;
    }

    public ResultadoExecucao somadoCom(Numero numero) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        return resultado.sucesso(new Numero(getValor().add(numero.getValor(),
                                                           mathContext)).setContexto(super.getContexto()));
    }

    public ResultadoExecucao subtraidoPor(Numero numero) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        return resultado.sucesso(new Numero(getValor().subtract(numero.getValor(),
                                                                mathContext)).setContexto(super.getContexto()));
    }

    public ResultadoExecucao multiplicadoPor(Numero numero) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        return resultado.sucesso(new Numero(getValor().multiply(numero.getValor(),
                                                                mathContext)).setContexto(super.getContexto()));
    }

    public ResultadoExecucao divididoPor(Numero numero) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        if (numero.getValor().equals(BigDecimal.ZERO)) return resultado.falha(new ErroDeExecucao("Divisão por zero",
                                                                                                 numero.getPosicaoInicial(),
                                                                                                 numero.getPosicaoFinal()));

        return resultado.sucesso(new Numero(getValor().divide(numero.getValor(),
                                                              mathContext)).setContexto(super.getContexto()));
    }

    public ResultadoExecucao potenciadoPor(Numero numero) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        double base = getValor().doubleValue();
        double expoente = numero.getValor().doubleValue();
        double solucao = Math.pow(base, expoente);
        if (Double.valueOf(solucao).equals(Double.NaN))
            return resultado.falha(new ErroDeExecucao("Potenciação inválida",
                                                      this.getPosicaoInicial(),
                                                      numero.getPosicaoFinal()));
        return resultado.sucesso(new Numero(new BigDecimal(solucao)).setContexto(super.getContexto()));
    }

    public ResultadoExecucao igual(Numero numero) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        Numero booleano = getValor().compareTo(numero.getValor()) == 0 ? Numero.TRUE : Numero.FALSE;
        booleano.setBooleano();

        return resultado.sucesso(booleano);
    }

    public ResultadoExecucao diferente(Numero numero) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        Numero booleano = getValor().compareTo(numero.getValor()) != 0 ? Numero.TRUE : Numero.FALSE;
        booleano.setBooleano();

        return resultado.sucesso(booleano);
    }

    public ResultadoExecucao maior(Numero numero) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        Numero booleano = getValor().compareTo(numero.getValor()) > 0 ? Numero.TRUE : Numero.FALSE;
        booleano.setBooleano();

        return resultado.sucesso(booleano);
    }

    public ResultadoExecucao menor(Numero numero) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        Numero booleano = getValor().compareTo(numero.getValor()) < 0 ? Numero.TRUE : Numero.FALSE;
        booleano.setBooleano();

        return resultado.sucesso(booleano);
    }

    public ResultadoExecucao maiorIgual(Numero numero) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        Numero booleano = getValor().compareTo(numero.getValor()) >= 0 ? Numero.TRUE : Numero.FALSE;
        booleano.setBooleano();

        return resultado.sucesso(booleano);
    }

    public ResultadoExecucao menorIgual(Numero numero) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        Numero booleano = getValor().compareTo(numero.getValor()) <= 0 ? Numero.TRUE : Numero.FALSE;
        booleano.setBooleano();

        return resultado.sucesso(booleano);
    }

    public ResultadoExecucao e(Numero numero) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        Numero booleano = (getValor().compareTo(BigDecimal.ZERO) != 0
                           && numero.getValor().compareTo(BigDecimal.ZERO) != 0)
                          ? Numero.TRUE : Numero.FALSE;
        booleano.setBooleano();

        return resultado.sucesso(booleano);
    }

    public ResultadoExecucao ou(Numero numero) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        Numero booleano = (getValor().compareTo(BigDecimal.ZERO) != 0
                           || numero.getValor().compareTo(BigDecimal.ZERO) != 0)
                          ? Numero.TRUE : Numero.FALSE;
        booleano.setBooleano();

        return resultado.sucesso(booleano);
    }

    public ResultadoExecucao xou(Numero numero) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        Numero booleano = (getValor().compareTo(BigDecimal.ZERO) == 0
                           && numero.getValor().compareTo(BigDecimal.ZERO) != 0) ||
                          (getValor().compareTo(BigDecimal.ZERO) != 0
                           && numero.getValor().compareTo(BigDecimal.ZERO) == 0)
                          ? Numero.TRUE : Numero.FALSE;
        booleano.setBooleano();

        return resultado.sucesso(booleano);
    }

    public ResultadoExecucao negado() {
        ResultadoExecucao resultado = new ResultadoExecucao();

        Numero booleano = getValor().compareTo(BigDecimal.ZERO) == 0 ? Numero.TRUE : Numero.FALSE;
        booleano.setBooleano();

        return resultado.sucesso(booleano);
    }

    public ResultadoExecucao paraNegativo() {
        ResultadoExecucao resultado = new ResultadoExecucao();
        return resultado.sucesso(new Numero(getValor().negate()).setContexto(super.getContexto()));
    }

    public Numero copiar() {
        Valor copia = new Numero(getValor()).setContexto(super.getContexto());
        copia.setPosicao(getPosicaoInicial(), getPosicaoFinal());
        return (Numero) copia;
    }

    public BigDecimal getValor() {
        return valor;
    }

    @Override
    public String toString() {
        String str = valor.stripTrailingZeros().toPlainString();
        if (booleano) {
            str = getValor().compareTo(BigDecimal.ZERO) == 0 ? "false" : "true";
        }
        return str;
    }
}
