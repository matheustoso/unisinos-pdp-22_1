package unisinos.pdp22_1.ga_t2.Linguagem.Nos;

import unisinos.pdp22_1.ga_t2.Linguagem.Contexto.Contexto;
import unisinos.pdp22_1.ga_t2.Linguagem.Interpreter.Visitante;
import unisinos.pdp22_1.ga_t2.Linguagem.Posicao.Posicao;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.ResultadoExecucao;

public abstract class No implements Elemento {
    private Posicao posicaoInicial;
    private Posicao posicaoFinal;

    public No(Posicao posicaoInicial, Posicao posicaoFinal) {
        this.posicaoInicial = posicaoInicial;
        this.posicaoFinal = posicaoFinal;
    }

    @Override
    public ResultadoExecucao aceitar(Visitante visitante, Contexto contexto) {
        return visitante.visitar(this, contexto);
    }

    public Posicao getPosicaoInicial() {
        return posicaoInicial;
    }

    public Posicao getPosicaoFinal() {
        return posicaoFinal;
    }
}
