package unisinos.pdp22_1.ga_t2.Linguagem.Nos;

import unisinos.pdp22_1.ga_t2.Linguagem.Tokens.Token;

public class VariavelAtribuicao extends Variavel{
    private No valor;

    public VariavelAtribuicao(Token nome, No valor) {
        super(nome);
        this.valor = valor;
    }

    public No getValor() {
        return valor;
    }

    public void setValor(No valor) {
        this.valor = valor;
    }
}
