package unisinos.pdp22_1.ga_t2.Linguagem.Nos;

import unisinos.pdp22_1.ga_t2.Linguagem.Contexto.Contexto;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.ResultadoExecucao;
import unisinos.pdp22_1.ga_t2.Linguagem.Tokens.Token;
import unisinos.pdp22_1.ga_t2.Linguagem.Interpreter.Visitante;

public abstract class Variavel extends No {
    private Token nome;

    public Variavel(Token nome) {
        super(nome.getPosicaoInicial(), nome.getPosicaoFinal());
        this.nome = nome;
    }

    public Token getNome() {
        return nome;
    }

    @Override
    public ResultadoExecucao aceitar(Visitante visitante, Contexto contexto) {
        return visitante.visitar(this, contexto);
    }

    @Override
    public String toString() {
        return nome.toString();
    }
}
