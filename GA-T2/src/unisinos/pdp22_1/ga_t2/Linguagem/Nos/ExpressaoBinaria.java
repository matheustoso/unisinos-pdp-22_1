package unisinos.pdp22_1.ga_t2.Linguagem.Nos;

import unisinos.pdp22_1.ga_t2.Linguagem.Contexto.Contexto;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.ResultadoExecucao;
import unisinos.pdp22_1.ga_t2.Linguagem.Tokens.Token;
import unisinos.pdp22_1.ga_t2.Linguagem.Interpreter.Visitante;

public class ExpressaoBinaria extends No {
    private No operandoEsquerdo;
    private Token operador;
    private No operandoDireito;

    public ExpressaoBinaria(No operandoEsquerdo, Token operador, No operandoDireito) {
        super(operandoEsquerdo.getPosicaoInicial(), operandoDireito.getPosicaoFinal());
        this.operandoEsquerdo = operandoEsquerdo;
        this.operador = operador;
        this.operandoDireito = operandoDireito;
    }

    @Override
    public String toString() {
        return "("+operandoEsquerdo.toString() + "," + operador.toString() + "," + operandoDireito.toString()+")";
    }

    @Override
    public ResultadoExecucao aceitar(Visitante visitante, Contexto contexto) {
        return visitante.visitar(this, contexto);
    }

    public No getOperandoEsquerdo() {
        return operandoEsquerdo;
    }

    public Token getOperador() {
        return operador;
    }

    public No getOperandoDireito() {
        return operandoDireito;
    }
}
