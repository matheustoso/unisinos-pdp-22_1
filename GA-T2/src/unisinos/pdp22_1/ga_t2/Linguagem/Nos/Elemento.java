package unisinos.pdp22_1.ga_t2.Linguagem.Nos;

import unisinos.pdp22_1.ga_t2.Linguagem.Contexto.Contexto;
import unisinos.pdp22_1.ga_t2.Linguagem.Interpreter.Visitante;

public interface Elemento {
    public abstract Object aceitar(Visitante visitante, Contexto contexto);
}
