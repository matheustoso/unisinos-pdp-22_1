package unisinos.pdp22_1.ga_t2.Linguagem.Nos;

import unisinos.pdp22_1.ga_t2.Linguagem.Contexto.Contexto;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.ResultadoExecucao;
import unisinos.pdp22_1.ga_t2.Linguagem.Tokens.Token;
import unisinos.pdp22_1.ga_t2.Linguagem.Interpreter.Visitante;

public class ExpressaoUnaria extends No {
    private Token operador;
    private No operando;

    public ExpressaoUnaria(Token operador, No operando) {
        super(operador.getPosicaoInicial(), operando.getPosicaoFinal());
        this.operador = operador;
        this.operando = operando;
    }

    @Override
    public String toString() {
        return "(" + operador + "," + operando + ')';
    }

    public Token getOperador() {
        return operador;
    }

    public No getOperando() {
        return operando;
    }

    @Override
    public ResultadoExecucao aceitar(Visitante visitante, Contexto contexto) {
        return visitante.visitar(this, contexto);
    }
}
