package unisinos.pdp22_1.ga_t2.Linguagem.Nos;

import unisinos.pdp22_1.ga_t2.Linguagem.Contexto.Contexto;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.ResultadoExecucao;
import unisinos.pdp22_1.ga_t2.Linguagem.Tokens.Token;
import unisinos.pdp22_1.ga_t2.Linguagem.Interpreter.Visitante;

public class OperandoNumero extends No {
    private Token token;

    public OperandoNumero(Token token) {
        super(token.getPosicaoInicial(), token.getPosicaoFinal());
        this.token = token;
    }

    @Override
    public String toString() {
        return token.toString();
    }

    @Override
    public ResultadoExecucao aceitar(Visitante visitante, Contexto contexto) {
        return visitante.visitar(this, contexto);
    }

    public Token getToken() {
        return token;
    }
}
