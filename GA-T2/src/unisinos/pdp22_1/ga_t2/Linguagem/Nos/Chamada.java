package unisinos.pdp22_1.ga_t2.Linguagem.Nos;

import unisinos.pdp22_1.ga_t2.Linguagem.Contexto.Contexto;
import unisinos.pdp22_1.ga_t2.Linguagem.Posicao.Posicao;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.ResultadoExecucao;
import unisinos.pdp22_1.ga_t2.Linguagem.Interpreter.Visitante;

import java.util.ArrayList;

public class Chamada extends No {
    private No nome;
    private ArrayList<No> parametros;

    public Chamada(No nome, ArrayList<No> parametros) {
        super(nome.getPosicaoInicial(), nome.getPosicaoFinal());
        this.nome = nome;
        this.parametros = parametros;
    }

    @Override
    public ResultadoExecucao aceitar(Visitante visitante, Contexto contexto) {
        return visitante.visitar(this, contexto);
    }

    @Override
    public Posicao getPosicaoFinal() {
        Posicao posicao = super.getPosicaoFinal();
        if (parametros.size() > 0) posicao = parametros.get(parametros.size() - 1).getPosicaoFinal();
        return posicao;
    }

    public No getNome() {
        return nome;
    }

    public ArrayList<No> getParametros() {
        return parametros;
    }

    @Override
    public String toString() {
        return  "(" +nome +"->"+ parametros.toString() +")";
    }
}
