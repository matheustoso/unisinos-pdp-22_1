package unisinos.pdp22_1.ga_t2.Linguagem.Resultados;

import unisinos.pdp22_1.ga_t2.Linguagem.Erros.Erro;

public class Resultado<V> {
    private V valor;
    private Erro erro;

    public Resultado(V valor, Erro erro) {
        this.valor = valor;
        this.erro = erro;
    }

    public V getValor() {
        return valor;
    }

    public Erro getErro() {
        return erro;
    }

    public void setValor(V valor) {
        this.valor = valor;
    }

    public void setErro(Erro erro) {
        this.erro = erro;
    }

    @Override
    public String toString() {
        String str = "";
        if (this.erro != null) {
            str = this.erro.toString();
        } else if (this.valor != null) {
            str = this.valor.toString();
        }
        return str;
    }
}
