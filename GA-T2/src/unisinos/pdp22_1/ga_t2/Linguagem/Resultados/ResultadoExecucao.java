package unisinos.pdp22_1.ga_t2.Linguagem.Resultados;

import unisinos.pdp22_1.ga_t2.Linguagem.Erros.Erro;
import unisinos.pdp22_1.ga_t2.Linguagem.Valores.Valor;

public class ResultadoExecucao extends Resultado<Valor> {
    public ResultadoExecucao() {
        super(null, null);
    }

    public Valor registrar(ResultadoExecucao resultado){
        if(resultado.getErro()!=null) super.setErro(resultado.getErro());
        return resultado.getValor();
    }

    public ResultadoExecucao sucesso(Valor valor){
        super.setValor(valor);
        return this;
    }

    public ResultadoExecucao falha(Erro erro){
        super.setErro(erro);
        return this;
    }
}
