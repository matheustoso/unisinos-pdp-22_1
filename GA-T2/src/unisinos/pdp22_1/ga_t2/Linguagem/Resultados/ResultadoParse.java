package unisinos.pdp22_1.ga_t2.Linguagem.Resultados;

import unisinos.pdp22_1.ga_t2.Linguagem.Erros.Erro;
import unisinos.pdp22_1.ga_t2.Linguagem.Nos.No;

public class ResultadoParse extends Resultado<No> {
    private int contAvanco;

    public ResultadoParse() {
        super(null, null);
        this.contAvanco = 0;
    }

    public void registrar() {
        contAvanco++;
    }

    public No registrar(ResultadoParse valor) {
        contAvanco+=valor.contAvanco;
        if (valor.getErro() != null) super.setErro(valor.getErro());
        return valor.getValor();
    }

    public ResultadoParse sucesso(No no) {
        super.setValor(no);
        return this;
    }

    public ResultadoParse falha(Erro erro) {
        if (super.getErro() == null || contAvanco == 0) super.setErro(erro);
        return this;
    }
}
