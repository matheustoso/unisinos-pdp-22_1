package unisinos.pdp22_1.ga_t2.Linguagem.Parser;

import unisinos.pdp22_1.ga_t2.Linguagem.Erros.ErroSintaxeInvalida;
import unisinos.pdp22_1.ga_t2.Linguagem.Nos.*;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.ResultadoParse;
import unisinos.pdp22_1.ga_t2.Linguagem.Tokens.PalavrasChave;
import unisinos.pdp22_1.ga_t2.Linguagem.Tokens.TiposToken;
import unisinos.pdp22_1.ga_t2.Linguagem.Tokens.Token;

import java.util.ArrayList;
import java.util.function.Supplier;

public class Parser {
    private ArrayList<Token> tokens;
    private int index;
    private Token tokenAtual;

    public Parser(ArrayList<Token> tokens) {
        this.tokens = tokens;
        this.index = -1;
        avancar();
    }

    public ResultadoParse parse() {
        ResultadoParse resultado = expressao();
        if (resultado.getErro() == null && !getTokenAtual().getTipo().equals(TiposToken.FIM_DO_ARQUIVO)) {
            return resultado.falha(new ErroSintaxeInvalida("Operador esperado",
                                                           getTokenAtual().getPosicaoInicial(),
                                                           getTokenAtual().getPosicaoFinal()));
        }
        return resultado;
    }

    public void avancar() {
        setIndex(getIndex() + 1);

        if (getIndex() < getTokens().size()) {
            setTokenAtual(getTokens().get(getIndex()));
        }
    }

    public ResultadoParse operando() {
        ResultadoParse resultado = new ResultadoParse();
        Token token = getTokenAtual();

        if (token.getTipo().equals(TiposToken.MAIS) || token.getTipo().equals(TiposToken.MENOS)) {
            resultado.registrar();
            avancar();
            No operando = resultado.registrar(operando());
            if (resultado.getErro() != null) return resultado;
            return resultado.sucesso(new ExpressaoUnaria(token, operando));

        } else if (token.getTipo().equals(TiposToken.NUMERO)) {
            resultado.registrar();
            avancar();
            return resultado.sucesso(new OperandoNumero(token));

        } else if (token.getTipo().equals(TiposToken.ID)) {
            resultado.registrar();
            avancar();
            return resultado.sucesso(new VariavelAcesso(token));
        } else if (token.getTipo().equals(TiposToken.PARENTESE_ESQUERDO)) {
            resultado.registrar();
            avancar();
            No expressao = resultado.registrar(expressao());
            if (resultado.getErro() != null) return resultado;
            if (getTokenAtual().getTipo().equals(TiposToken.PARENTESE_DIREITO)) {
                resultado.registrar();
                avancar();
                return resultado.sucesso(expressao);
            } else {
                return resultado.falha(new ErroSintaxeInvalida("')' esperado",
                                                               getTokenAtual().getPosicaoInicial(),
                                                               getTokenAtual().getPosicaoFinal()));
            }
        }

        return resultado.falha(new ErroSintaxeInvalida("Número, identificador, '+', '-' ou '(' esperado",
                                                       token.getPosicaoInicial(),
                                                       token.getPosicaoFinal()));
    }

    public ResultadoParse funcao() {
        ResultadoParse resultado = new ResultadoParse();
        No operando = resultado.registrar(operando());
        if (resultado.getErro() != null) return resultado;

        if (getTokenAtual().getTipo().equals(TiposToken.PARENTESE_ESQUERDO)) {
            resultado.registrar();
            avancar();
            ArrayList<No> parametros = new ArrayList<>();

            if (getTokenAtual().getTipo().equals(TiposToken.PARENTESE_DIREITO)) {
                resultado.registrar();
                avancar();
            } else {
                parametros.add(resultado.registrar(expressao()));
                if (resultado.getErro() != null) return resultado.falha(new ErroSintaxeInvalida(
                        "')', var, número, identificador, '+', '-', '!' ou '(' esperado",
                        getTokenAtual().getPosicaoInicial(),
                        getTokenAtual().getPosicaoFinal()));

                while(getTokenAtual().getTipo().equals(TiposToken.VIRGULA)){
                    resultado.registrar();
                    avancar();

                    parametros.add(resultado.registrar(expressao()));
                    if (resultado.getErro() != null) return resultado;
                }

                if (!getTokenAtual().getTipo().equals(TiposToken.PARENTESE_DIREITO)) {
                    return resultado.falha(new ErroSintaxeInvalida(
                            "',' ou ')' esperado",
                            getTokenAtual().getPosicaoInicial(),
                            getTokenAtual().getPosicaoFinal()));
                }

                resultado.registrar();
                avancar();
            }
            return resultado.sucesso(new Chamada(operando, parametros));
        }
        return resultado.sucesso(operando);
    }

    public ResultadoParse potencia() {
        ArrayList<TiposToken> tipos = new ArrayList<>();
        tipos.add(TiposToken.POT);

        return operacaoBinaria(this::funcao, tipos, this::termo);
    }

    public ResultadoParse termo() {
        ArrayList<TiposToken> tipos = new ArrayList<>();
        tipos.add(TiposToken.MULT);
        tipos.add(TiposToken.DIV);

        return operacaoBinaria(this::potencia, tipos);
    }

    public ResultadoParse expressao() {
        ResultadoParse resultado = new ResultadoParse();

        if (getTokenAtual().checaIgualdade(TiposToken.CHAVE, PalavrasChave.VARIAVEL.valor)) {
            resultado.registrar();
            avancar();

            if (!getTokenAtual().getTipo().equals(TiposToken.ID))
                return resultado.falha(new ErroSintaxeInvalida("Identificador esperado",
                                                               getTokenAtual().getPosicaoInicial(),
                                                               getTokenAtual().getPosicaoFinal()));

            Token nomeVariavel = getTokenAtual();
            resultado.registrar();
            avancar();

            if (!getTokenAtual().getTipo().equals(TiposToken.ATRIBUICAO))
                return resultado.falha(new ErroSintaxeInvalida("Atribuição '$' esperada",
                                                               getTokenAtual().getPosicaoInicial(),
                                                               getTokenAtual().getPosicaoFinal()));

            resultado.registrar();
            avancar();
            No valorVariavel = resultado.registrar(expressao());
            if (resultado.getErro() != null) return resultado;
            return resultado.sucesso(new VariavelAtribuicao(nomeVariavel, valorVariavel));

        }

        ArrayList<TiposToken> tipos = new ArrayList<>();
        tipos.add(TiposToken.OU);
        tipos.add(TiposToken.XOU);

        No no = resultado.registrar(operacaoBinaria(this::operacaoLogica, tipos));

        if (resultado.getErro() != null)
            return resultado.falha(new ErroSintaxeInvalida("')', var, número, identificador, '+', '-', '!' ou '(' esperado",
                                                           getTokenAtual().getPosicaoInicial(),
                                                           getTokenAtual().getPosicaoFinal()));

        return resultado.sucesso(no);
    }

    public ResultadoParse operacaoLogica(){
        ArrayList<TiposToken> tipos = new ArrayList<>();
        tipos.add(TiposToken.E);

        return operacaoBinaria(this::operacaoRelacional, tipos);
    }

    public ResultadoParse operacaoRelacional() {
        ResultadoParse resultado = new ResultadoParse();

        if (getTokenAtual().getTipo().equals(TiposToken.NEGACAO)) {
            Token negacao = getTokenAtual();
            resultado.registrar();
            avancar();

            No no = resultado.registrar(operacaoRelacional());
            if (resultado.getErro() != null)
                return resultado;

            return resultado.sucesso(new ExpressaoUnaria(negacao, no));
        }

        ArrayList<TiposToken> tipos = new ArrayList<>();
        tipos.add(TiposToken.IGUAL);
        tipos.add(TiposToken.DIFERENTE);
        tipos.add(TiposToken.MAIOR);
        tipos.add(TiposToken.MENOR);
        tipos.add(TiposToken.MAIOR_IGUAL);
        tipos.add(TiposToken.MENOR_IGUAl);

        No no = resultado.registrar(operacaoBinaria(this::operacaoAritmetica, tipos));

        if (resultado.getErro() != null)
            return resultado.falha(new ErroSintaxeInvalida("Número, identificador, '+', '-', '!' ou '(' esperado",
                                                           getTokenAtual().getPosicaoInicial(),
                                                           getTokenAtual().getPosicaoFinal()));

        return resultado.sucesso(no);
    }

    public ResultadoParse operacaoAritmetica() {
        ArrayList<TiposToken> tipos = new ArrayList<>();
        tipos.add(TiposToken.MAIS);
        tipos.add(TiposToken.MENOS);

        return operacaoBinaria(this::termo, tipos);
    }

    public ResultadoParse operacaoBinaria(Supplier<ResultadoParse> funcao, ArrayList<TiposToken> tipos) {
        ResultadoParse resultado = new ResultadoParse();
        No esquerdo;
        esquerdo = resultado.registrar(funcao.get());
        if (resultado.getErro() != null) return resultado;

        while (tipos.contains(getTokenAtual().getTipo())) {
            Token operador = getTokenAtual();
            resultado.registrar();
            avancar();
            No direito = resultado.registrar(funcao.get());
            if (resultado.getErro() != null) return resultado;
            esquerdo = new ExpressaoBinaria(esquerdo, operador, direito);
        }

        return resultado.sucesso(esquerdo);
    }

    public ResultadoParse operacaoBinaria(Supplier<ResultadoParse> funcaoEsquerda,
                                          ArrayList<TiposToken> tipos,
                                          Supplier<ResultadoParse> funcaoDireita) {
        ResultadoParse resultado = new ResultadoParse();
        No esquerdo;
        esquerdo = resultado.registrar(funcaoEsquerda.get());
        if (resultado.getErro() != null) return resultado;

        while (tipos.contains(getTokenAtual().getTipo())) {
            Token operador = getTokenAtual();
            resultado.registrar();
            avancar();
            No direito = resultado.registrar(funcaoDireita.get());
            if (resultado.getErro() != null) return resultado;
            esquerdo = new ExpressaoBinaria(esquerdo, operador, direito);
        }

        return resultado.sucesso(esquerdo);
    }

    public ArrayList<Token> getTokens() {
        return tokens;
    }

    private void setTokens(ArrayList<Token> tokens) {
        this.tokens = tokens;
    }

    public int getIndex() {
        return index;
    }

    private void setIndex(int index) {
        this.index = index;
    }

    public Token getTokenAtual() {
        return tokenAtual;
    }

    private void setTokenAtual(Token tokenAtual) {
        this.tokenAtual = tokenAtual;
    }
}
