package unisinos.pdp22_1.ga_t2.Linguagem.Interpreter;

import unisinos.pdp22_1.ga_t2.Linguagem.Contexto.Contexto;
import unisinos.pdp22_1.ga_t2.Linguagem.Nos.*;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.ResultadoExecucao;

public interface Visitante {
    ResultadoExecucao visitar(No no, Contexto contexto);
    ResultadoExecucao visitar(ExpressaoBinaria no, Contexto contexto);
    ResultadoExecucao visitar(ExpressaoUnaria no, Contexto contexto);
    ResultadoExecucao visitar(OperandoNumero no, Contexto contexto);
    ResultadoExecucao visitar(VariavelAcesso no, Contexto contexto);
    ResultadoExecucao visitar(VariavelAtribuicao no, Contexto contexto);
    ResultadoExecucao visitar(Chamada no, Contexto contexto);
}
