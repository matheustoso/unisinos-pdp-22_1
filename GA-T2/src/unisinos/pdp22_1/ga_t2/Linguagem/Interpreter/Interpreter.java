package unisinos.pdp22_1.ga_t2.Linguagem.Interpreter;

import unisinos.pdp22_1.ga_t2.Linguagem.Contexto.Contexto;
import unisinos.pdp22_1.ga_t2.Linguagem.Erros.ErroDeExecucao;
import unisinos.pdp22_1.ga_t2.Linguagem.Nos.*;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.ResultadoExecucao;
import unisinos.pdp22_1.ga_t2.Linguagem.Tokens.TiposToken;
import unisinos.pdp22_1.ga_t2.Linguagem.Valores.Funcao;
import unisinos.pdp22_1.ga_t2.Linguagem.Valores.Numero;
import unisinos.pdp22_1.ga_t2.Linguagem.Valores.Valor;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Interpreter implements Visitante {

    @Override
    public ResultadoExecucao visitar(No no, Contexto contexto) {
        switch (no.getClass().getSimpleName()) {
            case "ExpressaoBinaria":
                return visitar((ExpressaoBinaria) no, contexto);
            case "ExpressaoUnaria":
                return visitar((ExpressaoUnaria) no, contexto);
            case "OperandoNumero":
                return visitar((OperandoNumero) no, contexto);
            case "VariavelAcesso":
                return visitar((VariavelAcesso) no, contexto);
            case "VariavelAtribuicao":
                return visitar((VariavelAtribuicao) no, contexto);
            case "Chamada":
                return visitar((Chamada) no, contexto);
            default:
                System.out.println("Tipo de nó inexistente");
                return null;
        }
    }

    @Override
    public ResultadoExecucao visitar(ExpressaoBinaria no, Contexto contexto) {
        ResultadoExecucao resultadoExecucao = new ResultadoExecucao();

        Numero esquerdo = (Numero) resultadoExecucao.registrar(visitar(no.getOperandoEsquerdo(), contexto));
        Numero direito = (Numero) resultadoExecucao.registrar(visitar(no.getOperandoDireito(), contexto));
        Valor resultado = null;

        switch (no.getOperador().getTipo()) {
            case MAIS -> resultado = resultadoExecucao.registrar(esquerdo.somadoCom(direito));
            case MENOS -> resultado = resultadoExecucao.registrar(esquerdo.subtraidoPor(direito));
            case MULT -> resultado = resultadoExecucao.registrar(esquerdo.multiplicadoPor(direito));
            case DIV -> resultado = resultadoExecucao.registrar(esquerdo.divididoPor(direito));
            case POT -> resultado = resultadoExecucao.registrar(esquerdo.potenciadoPor(direito));
            case IGUAL -> resultado = resultadoExecucao.registrar(esquerdo.igual(direito));
            case DIFERENTE -> resultado = resultadoExecucao.registrar(esquerdo.diferente(direito));
            case MAIOR -> resultado = resultadoExecucao.registrar(esquerdo.maior(direito));
            case MENOR -> resultado = resultadoExecucao.registrar(esquerdo.menor(direito));
            case MAIOR_IGUAL -> resultado = resultadoExecucao.registrar(esquerdo.maiorIgual(direito));
            case MENOR_IGUAl -> resultado = resultadoExecucao.registrar(esquerdo.menorIgual(direito));
            case E -> resultado = resultadoExecucao.registrar(esquerdo.e(direito));
            case OU -> resultado = resultadoExecucao.registrar(esquerdo.ou(direito));
            case XOU -> resultado = resultadoExecucao.registrar(esquerdo.xou(direito));
            default -> resultadoExecucao.falha(new ErroDeExecucao("Operação inválida",
                                                                  no.getPosicaoInicial(),
                                                                  no.getPosicaoFinal()));
        }
        if (resultadoExecucao.getErro() != null) {
            return resultadoExecucao;
        }
        assert resultado != null;
        resultado.setPosicao(no.getPosicaoInicial(), no.getPosicaoFinal());
        return resultadoExecucao.sucesso(resultado);
    }

    @Override
    public ResultadoExecucao visitar(ExpressaoUnaria no, Contexto contexto) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        Valor numero = resultado.registrar(visitar(no.getOperando(), contexto));

        if (resultado.getErro() != null) return resultado;

        if (no.getOperador().getTipo().equals(TiposToken.MENOS))
            numero = resultado.registrar(numero.paraNegativo());
        else if (no.getOperador().getTipo().equals(TiposToken.NEGACAO))
            numero = resultado.registrar(numero.negado());
        if (resultado.getErro() != null) return resultado;

        numero.setPosicao(no.getPosicaoInicial(), no.getPosicaoFinal());

        return resultado.sucesso(numero);
    }

    @Override
    public ResultadoExecucao visitar(OperandoNumero no, Contexto contexto) {
        ResultadoExecucao resultado = new ResultadoExecucao();

        Numero numero = new Numero((BigDecimal) no.getToken().getValor());
        numero.setPosicao(no.getPosicaoInicial(), no.getPosicaoFinal());
        numero.setContexto(contexto);

        return resultado.sucesso(numero);
    }

    @Override
    public ResultadoExecucao visitar(VariavelAcesso no, Contexto contexto) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        String nome = no.getNome().getValor().toString();
        Valor valor = contexto.getSimbolos().obter(nome);

        if (valor == null) return resultado.falha(new ErroDeExecucao("Variável '" + nome + "' não foi declarada",
                                                                     no.getPosicaoInicial(),
                                                                     no.getPosicaoFinal()));

        Valor numero = valor.copiar();
        numero.setPosicao(no.getPosicaoInicial(), no.getPosicaoFinal());
        numero.setContexto(contexto);
        return resultado.sucesso(numero);
    }

    @Override
    public ResultadoExecucao visitar(VariavelAtribuicao no, Contexto contexto) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        String nome = no.getNome().getValor().toString();
        Valor numero = resultado.registrar(visitar(no.getValor(), contexto));

        if (numero == null) return resultado.falha(new ErroDeExecucao("Variável '" + nome + "' não foi declarada",
                                                                      no.getPosicaoInicial(),
                                                                      no.getPosicaoFinal()));

        contexto.getSimbolos().permanecer(nome, numero);

        return resultado.sucesso(numero);
    }

    @Override
    public ResultadoExecucao visitar(Chamada no, Contexto contexto) {
        ResultadoExecucao resultado = new ResultadoExecucao();
        ArrayList<Valor> parametros = new ArrayList<>();

        Funcao funcao = (Funcao) resultado.registrar(visitar(no.getNome(), contexto));
        if (resultado.getErro() != null) return resultado;
        funcao = funcao.copiar();
        funcao.setPosicao(no.getPosicaoInicial(), no.getPosicaoFinal());

        for (No parametro : no.getParametros()) {
            parametros.add(resultado.registrar(visitar(parametro, contexto)));
            if (resultado.getErro() != null) return resultado;
        }

        Numero valor = (Numero)resultado.registrar(funcao.executar(parametros));
        if (resultado.getErro() != null) return resultado;
        valor = valor.copiar();
        valor.setPosicao(no.getPosicaoInicial(), no.getPosicaoFinal());
        valor.setContexto(contexto);

        return resultado.sucesso(valor);
    }
}
