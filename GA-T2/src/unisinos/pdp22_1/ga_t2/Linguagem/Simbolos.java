package unisinos.pdp22_1.ga_t2.Linguagem;

import unisinos.pdp22_1.ga_t2.Linguagem.Valores.Valor;

import java.util.HashMap;

public class Simbolos {
    private HashMap<String, Valor> locais;
    private HashMap<String, Valor> herdados;

    public Simbolos(HashMap<String, Valor> locais) {
        this.locais = locais;
        this.herdados = new HashMap<>();
    }

    public Simbolos(Simbolos simbolos) {
        this.locais = simbolos.locais;
        this.herdados = simbolos.herdados;
    }

    public Valor obter(String nome) {
        Valor valor = locais.get(nome);
        if (valor == null && !herdados.isEmpty()) valor = herdados.get(nome);
        return valor;
    }

    public void permanecer(String nome, Valor valor){
        locais.put(nome, valor);
    }

    public void remover(String nome){
        locais.remove(nome);
    }
}