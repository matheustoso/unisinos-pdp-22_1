package unisinos.pdp22_1.ga_t2.Linguagem;

import unisinos.pdp22_1.ga_t2.Linguagem.Contexto.Contexto;
import unisinos.pdp22_1.ga_t2.Linguagem.Interpreter.Interpreter;
import unisinos.pdp22_1.ga_t2.Linguagem.Lexer.Lexer;
import unisinos.pdp22_1.ga_t2.Linguagem.Parser.Parser;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.Resultado;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.ResultadoExecucao;
import unisinos.pdp22_1.ga_t2.Linguagem.Resultados.ResultadoParse;
import unisinos.pdp22_1.ga_t2.Linguagem.Tokens.Token;
import unisinos.pdp22_1.ga_t2.Linguagem.Valores.Funcao;
import unisinos.pdp22_1.ga_t2.Linguagem.Valores.Numero;

import java.util.ArrayList;
import java.util.HashMap;

public class Language {
    private static Simbolos simbolosGlobais = new Simbolos(new HashMap<>());

    public static Resultado<ArrayList<Token>> executarLexer(String texto, String nomeArquivo) {
        Lexer lexer = new Lexer(texto, nomeArquivo);

        return lexer.criarTokens();
    }

    public static ResultadoParse executarParser(ArrayList<Token> tokens) {
        Parser parser = new Parser(tokens);
        return parser.parse();
    }

    public static void executar(String texto) {
        //Atualiza gramática
        simbolosGlobais.permanecer("true", Numero.TRUE);
        simbolosGlobais.permanecer("false", Numero.FALSE);
        simbolosGlobais.permanecer("if", Funcao._if);
        simbolosGlobais.permanecer("sum", Funcao.sum);
        simbolosGlobais.permanecer("avg", Funcao.avg);
        simbolosGlobais.permanecer("max", Funcao.max);
        simbolosGlobais.permanecer("min", Funcao.min);
        simbolosGlobais.permanecer("abs", Funcao.abs);
        simbolosGlobais.permanecer("sqrt", Funcao.sqrt);
        simbolosGlobais.permanecer("rand", Funcao.rand);

        //Lexer gera tokens
        Resultado<ArrayList<Token>> lex = executarLexer(texto, "shell");
        System.out.println(lex);
        if (lex.getErro() != null) return;

        //Parser gera árvore
        ResultadoParse parse = executarParser(lex.getValor());
        System.out.println(parse);
        if (parse.getErro() != null) return;

        //Interpreter executa o código
        Interpreter interpreter = new Interpreter();
        Contexto contexto = new Contexto("shell.linguagem");
        contexto.setSimbolos(simbolosGlobais);
        ResultadoExecucao interpret = parse.getValor().aceitar(interpreter, contexto);
        System.out.println(interpret);
    }
}
