package unisinos.pdp22_1.ga_t2.Linguagem.Contexto;

import unisinos.pdp22_1.ga_t2.Linguagem.Posicao.Posicao;
import unisinos.pdp22_1.ga_t2.Linguagem.Simbolos;

import java.util.HashMap;

public class Contexto {
    private String nome;
    private Posicao entrada;
    private Contexto pai;
    private Simbolos simbolos;

    public Contexto(String nome) {
        this.nome = nome;
        this.entrada = null;
        this.pai = null;
        this.simbolos = new Simbolos(new HashMap<>());
    }

    public Contexto(String nome, Contexto pai) {
        this.nome = nome;
        this.entrada = null;
        this.pai = pai;
        this.simbolos = new Simbolos(new HashMap<>());
    }

    public Contexto(String nome, Posicao entrada) {
        this.nome = nome;
        this.entrada = entrada;
        this.pai = null;
        this.simbolos = new Simbolos(new HashMap<>());
    }

    public Contexto(String nome, Posicao entrada, Contexto pai) {
        this.nome = nome;
        this.entrada = entrada;
        this.pai = pai;
        this.simbolos = new Simbolos(new HashMap<>());
    }

    public String getNome() {
        return nome;
    }

    public Posicao getEntrada() {
        return entrada;
    }

    public Contexto getPai() {
        return pai;
    }

    public Simbolos getSimbolos() {
        return simbolos;
    }

    public void setSimbolos(Simbolos simbolos) {
        this.simbolos = simbolos;
    }
}
