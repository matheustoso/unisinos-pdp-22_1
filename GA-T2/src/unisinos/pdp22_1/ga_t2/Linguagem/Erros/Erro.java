package unisinos.pdp22_1.ga_t2.Linguagem.Erros;

import unisinos.pdp22_1.ga_t2.Linguagem.Posicao.Posicao;

public class Erro {
    private final String nome;
    private final String mensagem;
    private final Posicao posicaoInicial;
    private final Posicao posicaoFinal;

    public String getNome() {
        return nome;
    }

    public String getMensagem() {
        return mensagem;
    }

    public Posicao getPosicaoInicial() {
        return posicaoInicial;
    }

    public Posicao getPosicaoFinal() {
        return posicaoFinal;
    }

    public Erro(String nome, String mensagem, Posicao posicaoInicial, Posicao posicaoFinal) {
        this.nome = nome;
        this.mensagem = mensagem;
        this.posicaoInicial = posicaoInicial;
        this.posicaoFinal = posicaoFinal;
    }

    @Override
    public String toString() {
        int linha = getPosicaoInicial().getLinha() + 1;
        StringBuilder str = new StringBuilder("Erro de: " + getNome() + ": " + getMensagem() +
                                              "\n\tArquivo: " + getPosicaoInicial().getNomeArquivo() +
                                              "\t|\tLinha: " + linha +
                                              "\t|\tPosição: (" + getPosicaoInicial().getColuna() + ", " +
                                              getPosicaoFinal().getColuna() + ")" +
                                              "\n\n\t" + getPosicaoInicial().getConteudoArquivo() + "\n\t");
        for (int i = 0; i < getPosicaoInicial().getColuna(); i++){
            str.append(" ");
        }
        for (int i = 0; i < getPosicaoFinal().getColuna() - getPosicaoInicial().getColuna(); i++){
            str.append("^");
        }
        return str.toString();
    }
}
