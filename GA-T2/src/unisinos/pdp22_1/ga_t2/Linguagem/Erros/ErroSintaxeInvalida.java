package unisinos.pdp22_1.ga_t2.Linguagem.Erros;

import unisinos.pdp22_1.ga_t2.Linguagem.Posicao.Posicao;

public class ErroSintaxeInvalida extends Erro {
    public ErroSintaxeInvalida(String mensagem, Posicao posicaoInicial, Posicao posicaoFinal) {
        super("Sintaxe Inválida", mensagem, posicaoInicial, posicaoFinal);
    }
}
