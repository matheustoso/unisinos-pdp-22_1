package unisinos.pdp22_1.ga_t2.Linguagem.Erros;

import unisinos.pdp22_1.ga_t2.Linguagem.Posicao.Posicao;

public class ErroDeExecucao extends Erro{
    public ErroDeExecucao(String mensagem, Posicao posicaoInicial, Posicao posicaoFinal) {
        super("Execução", mensagem, posicaoInicial, posicaoFinal);
    }
}
