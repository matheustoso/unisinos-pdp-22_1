package unisinos.pdp22_1.ga_t2.Linguagem.Erros;

import unisinos.pdp22_1.ga_t2.Linguagem.Posicao.Posicao;

public class ErroCharIlegal extends Erro {
    public ErroCharIlegal(String mensagem, Posicao posicaoInicial, Posicao posicaoFinal) {
        super("Char Ilegal", mensagem, posicaoInicial, posicaoFinal);
    }
}
