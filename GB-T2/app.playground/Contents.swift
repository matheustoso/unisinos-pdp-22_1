import Cocoa

//Executes the code written on the 'shell' multiline string literal.
print("🧿〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️SHELL〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️🧿\n")

let shell =
"""
var result = 1
for i = 1 to 6 then var result = result * i + rand(sum(i,result))


result
"""

Lang.compile(text: shell, fileName: "shell")

print("🧿〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️SHELL〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️🧿\n")

//Compiles the src.txt file on the resources playground package
print("🪬〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️SOURCE〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️🪬\n")

let fileName = "src"

guard let fileUrl = Bundle.main.path(forResource:fileName, ofType: ".txt") else {
    preconditionFailure("Source code missing. src.txt resource expected.")
} 

let fileText = try String(contentsOfFile: fileUrl, encoding: .utf8)

Lang.compile(text: fileText, fileName: fileName)
    
print("🪬〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️SOURCE〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️〰️🪬\n")
